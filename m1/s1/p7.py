from collections import defaultdict

loop = True
agenda = defaultdict(lambda: "Contactul nu exista")

def print_menu():     
    print(15 * "*" , "MENU" , 15 * "*")
    print("1 - Show agenda")
    print("2 - Add contact")
    print("3 - Add phone nr. to contact")
    print("4 - Find contact")
    print("5 - Delete contact")
    print("6 - Exit")   
    print(36 * "*")

    
while loop:         
    print_menu()  
    op = input("Your option: ")
     
    if op == '1':     
        print("Your agenda:\n")
        for val in agenda:
            print(val, " ", agenda[val], "\n")
            
    elif op == '2':
        name = input("Name: ")
        agenda[name] = ''
      
    elif op == '3':
        name = input("Name: ")
        phone_nr = input("Phone number: ")
        agenda[name] = phone_nr
        
    elif op == '4':
        name = input("Name to find: ")
        print(name, " ", agenda[name])
        
    elif op == '5':
         name = input("Name to delete: ")
         del agenda[name]
         print(name, " was deleted")
         
    elif op == '6':
        print("Exit")
        loop=False    
    else:
        print("Wrong option selection. Enter any key to try again..")
