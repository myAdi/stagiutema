def squared(val):
    return val**2

def func_5(func, items):
    my_list=[]
    for val in items:
        my_list.append(squared(val))
    return my_list

print(func_5(squared, (1, 1, 2, 3, 5, 8)))
