from collections import OrderedDict
from operator import itemgetter

def func_3(key, value):
    data = dict(zip(key, value))
    d = OrderedDict(sorted(data.items(), key=itemgetter(1)))
    return d
    

name = ['Gabi', 'Andreea', 'Paul']
years = [1993, 1989, 1997]

print(func_3(name, years))
