def func_6(x):
    if(isinstance(x, str)):
        return lambda a, b: 1 if len(a)>len(b) else (0 if len(a)==len(b) else -1)
    return lambda a, b: 1 if a>b else (0 if a==b else -1)
    

cmp1 = func_6(0)
print(cmp1(2, 3))

cmp2 = func_6('')
print(cmp2('ana are', 'adi are'))
